#!/bin/bash
set -eo pipefail

cd terraform

terraform get
terraform init -reconfigure
terraform validate
terraform plan
if [ $? -eq 0 ]; then
  echo "No changes, not applying"
elif [ $? -eq 1 ]; then
  echo "Terraform plan failed"
  exit 1
elif [ $? -eq 2 ]; then
  terraform apply -auto-approve
fi
