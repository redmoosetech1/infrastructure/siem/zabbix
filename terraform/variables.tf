variable "vsphere_user" {
  type = string
}

variable "vsphere_password" {
  type = string
}

variable "vsphere_server" {
  type = string
}

variable "datacenter" {
  type = string
}

variable "pool" {
  type = string
}

variable "admin_user" {
  type = string
}

variable "admin_password" {
  type = string
}

variable "ssh_pub_key" {
  type = string
}

variable "machines" {
  type = map(object({
    folder = string
    template = string
    hostname = string
    domain = string
    network = string
    ipv4_address = string
    ipv4_netmask = number
    ipv4_gateway = string
    dns_servers = list(string)
    dns_domain = string
    num_cpus = number
    num_cores_per_socket = number
    memory = number
    datastore_1 = string
    datastore_2 = string
    disks = list(object({
      label = string
      size = number
      unit_number = number
    }))
  }))
}
