
provision:
	terraform get
	terraform init -reconfigure
	ansible-playbook playbooks/provision.yml
